# Rust Node FFI N-Api Example

An example of using ffi-napi directly with Rust libraries.


```sh
$ cargo build
...

$ node example.js
{ num: 6765 }
```
